﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gestContainer
{
    public partial class supprimerDeclaration : Form
    {
        public supprimerDeclaration()
        {
            InitializeComponent();
        }

        private Form _mdiChild;

        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }

        private void accueilButton6_Click(object sender, EventArgs e)
        {
            MdiChild = new accueil();
        }

        private void consulterDeclarationButton1_Click(object sender, EventArgs e)
        {
            MdiChild = new consultationDeclaration();
        }

        private void saisirDeclarationButton1_Click(object sender, EventArgs e)
        {
            MdiChild = new saisirDeclaration();
        }

        private void modifierDeclarationButton3_Click(object sender, EventArgs e)
        {
            MdiChild = new modifierDeclaration();
        }

        private void FormSupprimer_FormClosing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }
    }
}
