﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace gestContainer
{
    class Docker
    {
        private string _codeDocker;
        private string _nomDocker;
        private string _prenomDocker;

        private bool _isNew;
        private static string _select = "select * from docker";
        public Docker()
        {
        }
        public string NomDocker { get => _nomDocker; set => _nomDocker = value; }
        public string PrenomDocker { get => _prenomDocker; set => _prenomDocker = value; }


        public static List<Docker> FetchAll()
        {

            List<Docker> collectionDeDocker = new List<Docker>();

            MySqlConnection msc = new MySqlConnection("Database=synapse;Data Source=localhost;User Id=root;Password=;Ssl Mode=None;charset=utf8");
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _select;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();

            while (jeuEnregistrements.Read())
            {
                Docker unDocker = new Docker();

                string codeDocker = jeuEnregistrements["codeDocker"].ToString();
                unDocker._codeDocker = codeDocker;

                string nomDocker = jeuEnregistrements["nomDocker"].ToString();
                unDocker._nomDocker = nomDocker;

                string prenomDocker = jeuEnregistrements["prenomDocker"].ToString();
                unDocker._prenomDocker = prenomDocker;

                unDocker._isNew = false;
                collectionDeDocker.Add(unDocker);
            }
            msc.Close();//La connexion est fermée

            return collectionDeDocker;
        }

        public static Docker Fetch(string codeDocker)
        {
            Docker unDocker = null;

                MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
                MySqlCommand commandSql = openConnection.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
                commandSql.CommandText = _select;//Définit la requete à utiliser
                commandSql.Parameters.Add(new MySqlParameter("?code", codeDocker));//Transmet un paramètre à utiliser lors de l'envoie de la requête. Il s’agit ici de l’identifiant transmis en paramètre.
                commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
                MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
                bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
                if (existEnregistrement)//Si l'enregistrement existe
                {//alors
                unDocker = new Docker();//Initialisation de la variable Contact
                unDocker._codeDocker = jeuEnregistrements["Code"].ToString();//Lecture d'un champ de l'enregistrement
                unDocker._nomDocker = jeuEnregistrements["nomDocker"].ToString();
                unDocker._prenomDocker = jeuEnregistrements["prenomDocker"].ToString();
                unDocker._isNew = false;
                }
                openConnection.Close();//fermeture de la connexion

            return unDocker;
        }
    }
}
