﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gestContainer
{
    public partial class modifierDeclaration : Form
    {
        public modifierDeclaration()
        {
            InitializeComponent();
        }

        private Form _mdiChild;
        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();

            }


        }

        private void accueilButton7_Click(object sender, EventArgs e)
        {
            MdiChild = new accueil();
        }

        private void consulterDeclarationButton6_Click(object sender, EventArgs e)
        {
            MdiChild = new consultationDeclaration();
        }

        private void saisirDeclarationButton1_Click(object sender, EventArgs e)
        {
            MdiChild = new saisirDeclaration();
        }

        private void supprimerDeclarationButton5_Click(object sender, EventArgs e)
        {
            MdiChild = new supprimerDeclaration();
        }

        private void FormModifier_FormClosing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }
    }
}
