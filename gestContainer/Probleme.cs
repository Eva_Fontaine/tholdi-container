﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace gestContainer
{
    class Probleme
    {
        private string _codeProbleme;
        private string _libelleProbleme;

        private bool _isNew;
        private static string _select = "select * from probleme";


        public Probleme()
        {
        }

        public string CodeProbleme { get => _codeProbleme; }
        public string LibelleProbleme { get => _libelleProbleme; set => _libelleProbleme = value; }

      
        public static List<Probleme> FetchAll()
        {

            List<Probleme> collectionDeProbleme = new List<Probleme>();

            MySqlConnection msc = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _select;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();

            while (jeuEnregistrements.Read())
            {

                Probleme unProbleme = new Probleme();

                string codeProbleme = jeuEnregistrements["codeProbleme"].ToString();
                unProbleme._codeProbleme = codeProbleme;

                string libelleProbleme = jeuEnregistrements["libelleProbleme"].ToString();
                unProbleme._libelleProbleme = libelleProbleme;

                unProbleme._isNew = false;
                collectionDeProbleme.Add(unProbleme);
            }
            msc.Close();//La connexion est fermée

            return collectionDeProbleme;
        }


        public static Probleme Fetch(string codeProbleme)
        {
            Probleme unProbleme = null;
            
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _select;//Définit la requete à utiliser
            commandSql.Parameters.Add(new MySqlParameter("?code", codeProbleme));//Transmet un paramètre à utiliser lors de l'envoie de la requête. Il s’agit ici de l’identifiant transmis en paramètre.
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                unProbleme = new Probleme();//Initialisation de la variable Contact
                unProbleme._codeProbleme = jeuEnregistrements["Code"].ToString();//Lecture d'un champ de l'enregistrement
                unProbleme._libelleProbleme = jeuEnregistrements["Libelle"].ToString();
                unProbleme._isNew = false;
            }
            openConnection.Close();//fermeture de la connexion


            return unProbleme;
        }
    }
}
