﻿
namespace gestContainer
{
    partial class modifierDeclaration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(modifierDeclaration));
            this.guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.saisirDeclarationButton1 = new Guna.UI2.WinForms.Guna2Button();
            this.accueilButton7 = new Guna.UI2.WinForms.Guna2Button();
            this.supprimerDeclarationButton5 = new Guna.UI2.WinForms.Guna2Button();
            this.consulterDeclarationButton6 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2PictureBox2 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.guna2ComboBox1 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.guna2ComboBox3 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.guna2GradientCircleButton1 = new Guna.UI2.WinForms.Guna2GradientCircleButton();
            this.guna2Panel2.SuspendLayout();
            this.guna2Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Panel2
            // 
            this.guna2Panel2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.guna2Panel2.Controls.Add(this.label1);
            this.guna2Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel2.Location = new System.Drawing.Point(199, 0);
            this.guna2Panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2Panel2.Name = "guna2Panel2";
            this.guna2Panel2.ShadowDecoration.Parent = this.guna2Panel2;
            this.guna2Panel2.Size = new System.Drawing.Size(923, 189);
            this.guna2Panel2.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe Script", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(182, 57);
            this.label1.MinimumSize = new System.Drawing.Size(100, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(552, 52);
            this.label1.TabIndex = 16;
            this.label1.Text = "MODIFIER DECLARATION";
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BackColor = System.Drawing.SystemColors.Highlight;
            this.guna2Panel1.Controls.Add(this.saisirDeclarationButton1);
            this.guna2Panel1.Controls.Add(this.accueilButton7);
            this.guna2Panel1.Controls.Add(this.supprimerDeclarationButton5);
            this.guna2Panel1.Controls.Add(this.consulterDeclarationButton6);
            this.guna2Panel1.Controls.Add(this.guna2PictureBox2);
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.ShadowDecoration.Parent = this.guna2Panel1;
            this.guna2Panel1.Size = new System.Drawing.Size(199, 577);
            this.guna2Panel1.TabIndex = 4;
            // 
            // saisirDeclarationButton1
            // 
            this.saisirDeclarationButton1.CheckedState.Parent = this.saisirDeclarationButton1;
            this.saisirDeclarationButton1.CustomImages.Parent = this.saisirDeclarationButton1;
            this.saisirDeclarationButton1.FillColor = System.Drawing.Color.LightSkyBlue;
            this.saisirDeclarationButton1.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saisirDeclarationButton1.ForeColor = System.Drawing.Color.Black;
            this.saisirDeclarationButton1.HoverState.Parent = this.saisirDeclarationButton1;
            this.saisirDeclarationButton1.Location = new System.Drawing.Point(20, 362);
            this.saisirDeclarationButton1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.saisirDeclarationButton1.Name = "saisirDeclarationButton1";
            this.saisirDeclarationButton1.ShadowDecoration.Parent = this.saisirDeclarationButton1;
            this.saisirDeclarationButton1.Size = new System.Drawing.Size(152, 57);
            this.saisirDeclarationButton1.TabIndex = 7;
            this.saisirDeclarationButton1.Text = "Ajouter déclaration";
            this.saisirDeclarationButton1.Click += new System.EventHandler(this.saisirDeclarationButton1_Click);
            // 
            // accueilButton7
            // 
            this.accueilButton7.CheckedState.Parent = this.accueilButton7;
            this.accueilButton7.CustomImages.Parent = this.accueilButton7;
            this.accueilButton7.FillColor = System.Drawing.Color.LightSkyBlue;
            this.accueilButton7.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accueilButton7.ForeColor = System.Drawing.Color.Black;
            this.accueilButton7.HoverState.Parent = this.accueilButton7;
            this.accueilButton7.Location = new System.Drawing.Point(20, 190);
            this.accueilButton7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.accueilButton7.Name = "accueilButton7";
            this.accueilButton7.ShadowDecoration.Parent = this.accueilButton7;
            this.accueilButton7.Size = new System.Drawing.Size(152, 57);
            this.accueilButton7.TabIndex = 6;
            this.accueilButton7.Text = "Accueil";
            this.accueilButton7.Click += new System.EventHandler(this.accueilButton7_Click);
            // 
            // supprimerDeclarationButton5
            // 
            this.supprimerDeclarationButton5.CheckedState.Parent = this.supprimerDeclarationButton5;
            this.supprimerDeclarationButton5.CustomImages.Parent = this.supprimerDeclarationButton5;
            this.supprimerDeclarationButton5.FillColor = System.Drawing.Color.LightSkyBlue;
            this.supprimerDeclarationButton5.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supprimerDeclarationButton5.ForeColor = System.Drawing.Color.Black;
            this.supprimerDeclarationButton5.HoverState.Parent = this.supprimerDeclarationButton5;
            this.supprimerDeclarationButton5.Location = new System.Drawing.Point(20, 452);
            this.supprimerDeclarationButton5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.supprimerDeclarationButton5.Name = "supprimerDeclarationButton5";
            this.supprimerDeclarationButton5.ShadowDecoration.Parent = this.supprimerDeclarationButton5;
            this.supprimerDeclarationButton5.Size = new System.Drawing.Size(152, 57);
            this.supprimerDeclarationButton5.TabIndex = 4;
            this.supprimerDeclarationButton5.Text = "Supprimer déclaration";
            this.supprimerDeclarationButton5.Click += new System.EventHandler(this.supprimerDeclarationButton5_Click);
            // 
            // consulterDeclarationButton6
            // 
            this.consulterDeclarationButton6.CheckedState.Parent = this.consulterDeclarationButton6;
            this.consulterDeclarationButton6.CustomImages.Parent = this.consulterDeclarationButton6;
            this.consulterDeclarationButton6.FillColor = System.Drawing.Color.LightSkyBlue;
            this.consulterDeclarationButton6.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consulterDeclarationButton6.ForeColor = System.Drawing.Color.Black;
            this.consulterDeclarationButton6.HoverState.Parent = this.consulterDeclarationButton6;
            this.consulterDeclarationButton6.Location = new System.Drawing.Point(20, 273);
            this.consulterDeclarationButton6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.consulterDeclarationButton6.Name = "consulterDeclarationButton6";
            this.consulterDeclarationButton6.ShadowDecoration.Parent = this.consulterDeclarationButton6;
            this.consulterDeclarationButton6.Size = new System.Drawing.Size(152, 57);
            this.consulterDeclarationButton6.TabIndex = 3;
            this.consulterDeclarationButton6.Text = "Consultation Déclaration";
            this.consulterDeclarationButton6.Click += new System.EventHandler(this.consulterDeclarationButton6_Click);
            // 
            // guna2PictureBox2
            // 
            this.guna2PictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox2.Image")));
            this.guna2PictureBox2.Location = new System.Drawing.Point(9, 25);
            this.guna2PictureBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2PictureBox2.Name = "guna2PictureBox2";
            this.guna2PictureBox2.ShadowDecoration.Parent = this.guna2PictureBox2;
            this.guna2PictureBox2.Size = new System.Drawing.Size(155, 116);
            this.guna2PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox2.TabIndex = 1;
            this.guna2PictureBox2.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.groupBox1.Controls.Add(this.guna2ComboBox1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.guna2ComboBox3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.guna2GradientCircleButton1);
            this.groupBox1.Location = new System.Drawing.Point(204, 190);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(918, 387);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            // 
            // guna2ComboBox1
            // 
            this.guna2ComboBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.guna2ComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox1.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ComboBox1.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox1.FocusedState.Parent = this.guna2ComboBox1;
            this.guna2ComboBox1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox1.HoverState.Parent = this.guna2ComboBox1;
            this.guna2ComboBox1.ItemHeight = 30;
            this.guna2ComboBox1.ItemsAppearance.Parent = this.guna2ComboBox1;
            this.guna2ComboBox1.Location = new System.Drawing.Point(497, 172);
            this.guna2ComboBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2ComboBox1.Name = "guna2ComboBox1";
            this.guna2ComboBox1.ShadowDecoration.Parent = this.guna2ComboBox1;
            this.guna2ComboBox1.Size = new System.Drawing.Size(261, 36);
            this.guna2ComboBox1.TabIndex = 30;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(165, 172);
            this.label3.MinimumSize = new System.Drawing.Size(100, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 52);
            this.label3.TabIndex = 29;
            this.label3.Text = "Urgence";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(165, 117);
            this.label2.MinimumSize = new System.Drawing.Size(100, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 52);
            this.label2.TabIndex = 28;
            this.label2.Text = "Commentaire";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(497, 120);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(261, 20);
            this.textBox1.TabIndex = 27;
            // 
            // guna2ComboBox3
            // 
            this.guna2ComboBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.guna2ComboBox3.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox3.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ComboBox3.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox3.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox3.FocusedState.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox3.HoverState.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.ItemHeight = 30;
            this.guna2ComboBox3.ItemsAppearance.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.Location = new System.Drawing.Point(497, 46);
            this.guna2ComboBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2ComboBox3.Name = "guna2ComboBox3";
            this.guna2ComboBox3.ShadowDecoration.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.Size = new System.Drawing.Size(261, 36);
            this.guna2ComboBox3.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(165, 46);
            this.label4.MinimumSize = new System.Drawing.Size(100, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(270, 52);
            this.label4.TabIndex = 16;
            this.label4.Text = "Veuillez choisir le numéro de déclaration";
            // 
            // guna2GradientCircleButton1
            // 
            this.guna2GradientCircleButton1.CheckedState.Parent = this.guna2GradientCircleButton1;
            this.guna2GradientCircleButton1.CustomImages.Parent = this.guna2GradientCircleButton1;
            this.guna2GradientCircleButton1.FillColor2 = System.Drawing.SystemColors.HotTrack;
            this.guna2GradientCircleButton1.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2GradientCircleButton1.ForeColor = System.Drawing.Color.White;
            this.guna2GradientCircleButton1.HoverState.Parent = this.guna2GradientCircleButton1;
            this.guna2GradientCircleButton1.Location = new System.Drawing.Point(666, 249);
            this.guna2GradientCircleButton1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2GradientCircleButton1.Name = "guna2GradientCircleButton1";
            this.guna2GradientCircleButton1.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2GradientCircleButton1.ShadowDecoration.Parent = this.guna2GradientCircleButton1;
            this.guna2GradientCircleButton1.Size = new System.Drawing.Size(92, 44);
            this.guna2GradientCircleButton1.TabIndex = 9;
            this.guna2GradientCircleButton1.Text = "Valider";
            // 
            // modifierDeclaration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1122, 577);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.guna2Panel2);
            this.Controls.Add(this.guna2Panel1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "modifierDeclaration";
            this.Text = "ModifierDéclaration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormModifier_FormClosing);
            this.guna2Panel2.ResumeLayout(false);
            this.guna2Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2Button saisirDeclarationButton1;
        private Guna.UI2.WinForms.Guna2Button accueilButton7;
        private Guna.UI2.WinForms.Guna2Button supprimerDeclarationButton5;
        private Guna.UI2.WinForms.Guna2Button consulterDeclarationButton6;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2GradientCircleButton guna2GradientCircleButton1;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox1;
    }
}