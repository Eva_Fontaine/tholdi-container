﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gestContainer
{
    public partial class saisirDeclaration : Form
    {
        public saisirDeclaration()
        {
            InitializeComponent();
        }
       

        private void accueilButton6_Click(object sender, EventArgs e)
        {
            this.Hide();
            new accueil().ShowDialog();
            this.Close();
        }

        private void consultationDeclarationButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
            new consultationDeclaration().ShowDialog();
            this.Close();
        }

        private void supprimerDeclarationButton5_Click(object sender, EventArgs e)
        {
            this.Hide();
            new supprimerDeclaration().ShowDialog();
            this.Close();
        }

        private void modifierDeclarationButton7_Click(object sender, EventArgs e)
        {
            this.Hide();
            new modifierDeclaration().ShowDialog();
            this.Close();
        }

        private void FormSaisir_FormClosing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }
    }
}
