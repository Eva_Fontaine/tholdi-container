﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gestContainer
{
    class Donnees
    {
        private static List<Probleme> _unProbleme;
        private static List<Conteneur> _unConteneur;
        private static List<Docker> _unDocker;

        /// <summary>
        /// Obtient la liste des activités de formation
        /// </summary>
        /// <remarks>Les activités contiennent les actions de formations et les sessions de formations</remarks>
        public static List<Probleme> CollectionProbleme
        {
            get
            {
                if (_unProbleme == null)
                {
                    _unProbleme = (List<Probleme>)Persistances.ChargerDonnees("Probleme");
                    if (_unProbleme == null)
                        _unProbleme = new List<Probleme>();
                }
                return Donnees._unProbleme;
            }
            set { Donnees._unProbleme = value; }
        }
        /// <summary>
        /// Obtient la liste des agents
        /// </summary>
        public static List<Conteneur> CollectionConteneur
        {
            get
            {
                if (_unConteneur == null)
                {
                    _unConteneur = (List<Conteneur>)Persistances.ChargerDonnees("Conteneur");
                    if (_unConteneur == null)
                        _unConteneur = new List<Conteneur>();
                }
                return Donnees._unConteneur;
            }
        }
        /// <summary>
        /// Obtient la liste des lieux
        /// </summary>
        public static List<Docker> CollectionDocker
        {
            get
            {
                if (_unDocker == null)
                {
                    _unDocker = (List<Docker>)Persistances.ChargerDonnees("Docker");
                    if (_unDocker == null)
                        _unDocker = new List<Docker>();
                }
                return Donnees._unDocker;
            }
        }

        /// <summary>
        /// Sérialise l'intégralité des données de l'application
        /// </summary>
        public static void SauvegardeDonnees()
        {
            Persistances.SauvegarderDonnees("Probleme", _unProbleme);
            Persistances.SauvegarderDonnees("Conteneur", _unConteneur);
            Persistances.SauvegarderDonnees("Docker", _unDocker);

        }
    }
}
