﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gestContainer
{
    public partial class consultationDeclaration : Form
    {
        public consultationDeclaration()
        {
            InitializeComponent();
        }

    

        private void accueilButton6_Click(object sender, EventArgs e)
        {
            this.Hide();
            new accueil().ShowDialog();
            this.Close();
        }

        private void saisirDeclarationButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new saisirDeclaration().ShowDialog();
            this.Close(); 
        }

        private void supprimerDeclarationButton4_Click(object sender, EventArgs e)
        {
            this.Hide();
            new supprimerDeclaration().ShowDialog();
            this.Close();

            
        }

        private void modifierDeclarationButton3_Click(object sender, EventArgs e)
        {

            this.Hide();
            new modifierDeclaration().ShowDialog();
            this.Close();
        }



        private void ComboBoxListeDeclaration_SelectedIndexChanged(object sender, EventArgs e)
        {
                        DataGridViewDeclaration.DataSource = Declaration.FetchAll();
        }


        private void FormConsultation_FormClosing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }
    }
}
