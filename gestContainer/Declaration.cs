﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace gestContainer
{
    class Declaration
    {
        private string _codeDeclaration;
        private string _commentaireDeclaration;
        private DateTime _dateDeclaration;
        private string _urgence;
        private string _traite;

        private Probleme _unProbleme;
        private Conteneur _unConteneur;
        private Docker _unDocker;

        private bool _isNew;
        private static string _select = "select * from declaration";
        private static string _insert = "insert into declaration (numContainer,codeProbleme,commentaireDeclaration,dateDeclaration,urgence,traite,docker)" + "values (?numContainer,?codeProbleme,?commentaireDeclaration,?dateDeclaration,?urgence,?traite,?docker)";
        private static string _update = "update declaration set commentaireDeclaration = ?commentaireDeclaration, urgence = ?urgence  where codeDeclaration=?codeDeclaration ";
        private static string _delete = "delete from declaration where codeDeclaration=?codeDeclaration";


        public Declaration()
        {
        }

        public string codeDeclaration
        {
            get { return _codeDeclaration; }
            set { _codeDeclaration = value; }
        }

        public string commentaireDeclaration
        {
            get { return _commentaireDeclaration; }
            set { _commentaireDeclaration = value; }
        }

        public DateTime dateDeclaration
        {
            get { return _dateDeclaration; }
            set { _dateDeclaration = value; }
        }

        public string urgence
        {
            get { return _urgence; }
            set { _urgence = value; }
        }

        public string traite
        {
            get { return _traite; }
            set { _traite = value; }
        }

        
        public Probleme unProbleme
        {
            get { return _unProbleme; }
            set { _unProbleme = value; }
        }

        public Conteneur unConteneur
        {
            get { return _unConteneur; }
            set { _unConteneur = value; }
        }

        public Docker unDocker
        {
            get { return _unDocker; }
            set { _unDocker = value; }
        }



        

        public void Save()
        {
            if (_isNew == true)
            {
                insert();
                _isNew = false;
            }
            else 
            {
                update();
            }
        }

        private void insert()
        {
            MySqlConnection msc = new MySqlConnection("Database=synapse;Data Source=localhost;User Id=root;Password=;Ssl Mode=None;charset=utf8");
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _insert;
            commandSql.ExecuteReader();
        }

        private void update()
        {
            MySqlConnection msc = new MySqlConnection("Database=synapse;Data Source=localhost;User Id=root;Password=;Ssl Mode=None;charset=utf8");
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _update;
            commandSql.ExecuteNonQuery();
        }

        private void delete()
        {
            MySqlConnection msc = new MySqlConnection("Database=synapse;Data Source=localhost;User Id=root;Password=;Ssl Mode=None;charset=utf8");
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _delete;
            commandSql.ExecuteNonQuery();
        }

        public static List<Declaration> FetchAll()
        {
            List<Declaration> collectionDeclaration = new List<Declaration>();

            MySqlConnection msc = new MySqlConnection("Database=synapse;Data Source=localhost;User Id=root;Password=;Ssl Mode=None;charset=utf8");
            msc.Open();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _select;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();

            while (jeuEnregistrements.Read())
            {
                Declaration uneDeclaration = new Declaration();

                uneDeclaration._unProbleme = Probleme.Fetch(jeuEnregistrements["codeProbleme"].ToString());

                uneDeclaration._unConteneur = Conteneur.Fetch(jeuEnregistrements["numConteneur"].ToString());

                uneDeclaration._unDocker = Docker.Fetch(jeuEnregistrements["codeDocker"].ToString());

                string commentaireDeclaration = jeuEnregistrements["commentaireDeclaration"].ToString();
                uneDeclaration._commentaireDeclaration = commentaireDeclaration;

                string urgence = jeuEnregistrements["urgence"].ToString();
                uneDeclaration._urgence = urgence;

                string traite = jeuEnregistrements["traite"].ToString();
                uneDeclaration._traite = traite;

                collectionDeclaration.Add(uneDeclaration);
            }
            msc.Close();//La connexion est fermée




            return collectionDeclaration;

        }

        public static Declaration Fetch(string codeDeclaration)
        {
            Declaration uneDeclaration = null;

            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _select;//Définit la requete à utiliser
            commandSql.Parameters.Add(new MySqlParameter("?code", codeDeclaration));//Transmet un paramètre à utiliser lors de l'envoie de la requête. Il s’agit ici de l’identifiant transmis en paramètre.
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                uneDeclaration = new Declaration();//Initialisation de la variable Contact
                uneDeclaration._codeDeclaration = jeuEnregistrements["Code"].ToString();//Lecture d'un champ de l'enregistrement
                uneDeclaration._commentaireDeclaration = jeuEnregistrements["Commentaire"].ToString();
                uneDeclaration._dateDeclaration = Convert.ToDateTime(jeuEnregistrements["DateDeclaration"].ToString());
                uneDeclaration._urgence = jeuEnregistrements["Urgence"].ToString();
                uneDeclaration._traite = jeuEnregistrements["Traite"].ToString();
                uneDeclaration._isNew = false;
            }
            openConnection.Close();//fermeture de la connexion


            return uneDeclaration;
        }



    }
}
