﻿
namespace gestContainer
{
    partial class saisirDeclaration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(saisirDeclaration));
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.consultationDeclarationButton2 = new Guna.UI2.WinForms.Guna2Button();
            this.modifierDeclarationButton7 = new Guna.UI2.WinForms.Guna2Button();
            this.supprimerDeclarationButton5 = new Guna.UI2.WinForms.Guna2Button();
            this.accueilButton6 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2PictureBox2 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.guna2ComboBox1 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2ComboBox3 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2ComboBox4 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.guna2GradientCircleButton1 = new Guna.UI2.WinForms.Guna2GradientCircleButton();
            this.guna2Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).BeginInit();
            this.guna2Panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BackColor = System.Drawing.SystemColors.Highlight;
            this.guna2Panel1.Controls.Add(this.consultationDeclarationButton2);
            this.guna2Panel1.Controls.Add(this.modifierDeclarationButton7);
            this.guna2Panel1.Controls.Add(this.supprimerDeclarationButton5);
            this.guna2Panel1.Controls.Add(this.accueilButton6);
            this.guna2Panel1.Controls.Add(this.guna2PictureBox2);
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.ShadowDecoration.Parent = this.guna2Panel1;
            this.guna2Panel1.Size = new System.Drawing.Size(184, 655);
            this.guna2Panel1.TabIndex = 2;
            // 
            // consultationDeclarationButton2
            // 
            this.consultationDeclarationButton2.CheckedState.Parent = this.consultationDeclarationButton2;
            this.consultationDeclarationButton2.CustomImages.Parent = this.consultationDeclarationButton2;
            this.consultationDeclarationButton2.FillColor = System.Drawing.Color.LightSkyBlue;
            this.consultationDeclarationButton2.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consultationDeclarationButton2.ForeColor = System.Drawing.Color.Black;
            this.consultationDeclarationButton2.HoverState.Parent = this.consultationDeclarationButton2;
            this.consultationDeclarationButton2.Location = new System.Drawing.Point(9, 282);
            this.consultationDeclarationButton2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.consultationDeclarationButton2.Name = "consultationDeclarationButton2";
            this.consultationDeclarationButton2.ShadowDecoration.Parent = this.consultationDeclarationButton2;
            this.consultationDeclarationButton2.Size = new System.Drawing.Size(152, 57);
            this.consultationDeclarationButton2.TabIndex = 8;
            this.consultationDeclarationButton2.Text = "Consultation déclaration";
            this.consultationDeclarationButton2.Click += new System.EventHandler(this.consultationDeclarationButton2_Click);
            // 
            // modifierDeclarationButton7
            // 
            this.modifierDeclarationButton7.CheckedState.Parent = this.modifierDeclarationButton7;
            this.modifierDeclarationButton7.CustomImages.Parent = this.modifierDeclarationButton7;
            this.modifierDeclarationButton7.FillColor = System.Drawing.Color.LightSkyBlue;
            this.modifierDeclarationButton7.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifierDeclarationButton7.ForeColor = System.Drawing.Color.Black;
            this.modifierDeclarationButton7.HoverState.Parent = this.modifierDeclarationButton7;
            this.modifierDeclarationButton7.Location = new System.Drawing.Point(9, 448);
            this.modifierDeclarationButton7.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.modifierDeclarationButton7.Name = "modifierDeclarationButton7";
            this.modifierDeclarationButton7.ShadowDecoration.Parent = this.modifierDeclarationButton7;
            this.modifierDeclarationButton7.Size = new System.Drawing.Size(152, 57);
            this.modifierDeclarationButton7.TabIndex = 6;
            this.modifierDeclarationButton7.Text = "Modifier déclaration";
            this.modifierDeclarationButton7.Click += new System.EventHandler(this.modifierDeclarationButton7_Click);
            // 
            // supprimerDeclarationButton5
            // 
            this.supprimerDeclarationButton5.CheckedState.Parent = this.supprimerDeclarationButton5;
            this.supprimerDeclarationButton5.CustomImages.Parent = this.supprimerDeclarationButton5;
            this.supprimerDeclarationButton5.FillColor = System.Drawing.Color.LightSkyBlue;
            this.supprimerDeclarationButton5.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supprimerDeclarationButton5.ForeColor = System.Drawing.Color.Black;
            this.supprimerDeclarationButton5.HoverState.Parent = this.supprimerDeclarationButton5;
            this.supprimerDeclarationButton5.Location = new System.Drawing.Point(9, 363);
            this.supprimerDeclarationButton5.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.supprimerDeclarationButton5.Name = "supprimerDeclarationButton5";
            this.supprimerDeclarationButton5.ShadowDecoration.Parent = this.supprimerDeclarationButton5;
            this.supprimerDeclarationButton5.Size = new System.Drawing.Size(152, 57);
            this.supprimerDeclarationButton5.TabIndex = 4;
            this.supprimerDeclarationButton5.Text = "Supprimer déclaration";
            this.supprimerDeclarationButton5.Click += new System.EventHandler(this.supprimerDeclarationButton5_Click);
            // 
            // accueilButton6
            // 
            this.accueilButton6.CheckedState.Parent = this.accueilButton6;
            this.accueilButton6.CustomImages.Parent = this.accueilButton6;
            this.accueilButton6.FillColor = System.Drawing.Color.LightSkyBlue;
            this.accueilButton6.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.accueilButton6.ForeColor = System.Drawing.Color.Black;
            this.accueilButton6.HoverState.Parent = this.accueilButton6;
            this.accueilButton6.Location = new System.Drawing.Point(9, 204);
            this.accueilButton6.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.accueilButton6.Name = "accueilButton6";
            this.accueilButton6.ShadowDecoration.Parent = this.accueilButton6;
            this.accueilButton6.Size = new System.Drawing.Size(152, 57);
            this.accueilButton6.TabIndex = 3;
            this.accueilButton6.Text = "Accueil";
            this.accueilButton6.Click += new System.EventHandler(this.accueilButton6_Click);
            // 
            // guna2PictureBox2
            // 
            this.guna2PictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox2.Image")));
            this.guna2PictureBox2.Location = new System.Drawing.Point(9, 25);
            this.guna2PictureBox2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2PictureBox2.Name = "guna2PictureBox2";
            this.guna2PictureBox2.ShadowDecoration.Parent = this.guna2PictureBox2;
            this.guna2PictureBox2.Size = new System.Drawing.Size(155, 116);
            this.guna2PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox2.TabIndex = 1;
            this.guna2PictureBox2.TabStop = false;
            // 
            // guna2Panel2
            // 
            this.guna2Panel2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.guna2Panel2.Controls.Add(this.label1);
            this.guna2Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel2.Location = new System.Drawing.Point(184, 0);
            this.guna2Panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2Panel2.Name = "guna2Panel2";
            this.guna2Panel2.ShadowDecoration.Parent = this.guna2Panel2;
            this.guna2Panel2.Size = new System.Drawing.Size(938, 189);
            this.guna2Panel2.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe Script", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(182, 57);
            this.label1.MinimumSize = new System.Drawing.Size(100, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(552, 52);
            this.label1.TabIndex = 16;
            this.label1.Text = "AJOUTER DECLARATION";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(371, 323);
            this.label4.MinimumSize = new System.Drawing.Size(100, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 52);
            this.label4.TabIndex = 16;
            this.label4.Text = "Commentaire";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(371, 384);
            this.label3.MinimumSize = new System.Drawing.Size(100, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(140, 52);
            this.label3.TabIndex = 18;
            this.label3.Text = "Urgence";
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(371, 258);
            this.label6.MinimumSize = new System.Drawing.Size(100, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 52);
            this.label6.TabIndex = 20;
            this.label6.Text = "Num conteneur";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Segoe Script", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(371, 453);
            this.label7.MinimumSize = new System.Drawing.Size(100, 40);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(167, 52);
            this.label7.TabIndex = 21;
            this.label7.Text = "Type de problème";
            // 
            // guna2ComboBox1
            // 
            this.guna2ComboBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.guna2ComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox1.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ComboBox1.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox1.FocusedState.Parent = this.guna2ComboBox1;
            this.guna2ComboBox1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox1.HoverState.Parent = this.guna2ComboBox1;
            this.guna2ComboBox1.ItemHeight = 30;
            this.guna2ComboBox1.ItemsAppearance.Parent = this.guna2ComboBox1;
            this.guna2ComboBox1.Location = new System.Drawing.Point(657, 258);
            this.guna2ComboBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2ComboBox1.Name = "guna2ComboBox1";
            this.guna2ComboBox1.ShadowDecoration.Parent = this.guna2ComboBox1;
            this.guna2ComboBox1.Size = new System.Drawing.Size(261, 36);
            this.guna2ComboBox1.TabIndex = 23;
            // 
            // guna2ComboBox3
            // 
            this.guna2ComboBox3.BackColor = System.Drawing.SystemColors.ControlLight;
            this.guna2ComboBox3.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox3.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ComboBox3.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox3.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox3.FocusedState.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox3.HoverState.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.ItemHeight = 30;
            this.guna2ComboBox3.ItemsAppearance.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.Location = new System.Drawing.Point(657, 384);
            this.guna2ComboBox3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2ComboBox3.Name = "guna2ComboBox3";
            this.guna2ComboBox3.ShadowDecoration.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.Size = new System.Drawing.Size(261, 36);
            this.guna2ComboBox3.TabIndex = 24;
            // 
            // guna2ComboBox4
            // 
            this.guna2ComboBox4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.guna2ComboBox4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox4.FillColor = System.Drawing.SystemColors.ActiveCaption;
            this.guna2ComboBox4.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox4.FocusedState.Parent = this.guna2ComboBox4;
            this.guna2ComboBox4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox4.HoverState.Parent = this.guna2ComboBox4;
            this.guna2ComboBox4.ItemHeight = 30;
            this.guna2ComboBox4.ItemsAppearance.Parent = this.guna2ComboBox4;
            this.guna2ComboBox4.Location = new System.Drawing.Point(657, 453);
            this.guna2ComboBox4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2ComboBox4.Name = "guna2ComboBox4";
            this.guna2ComboBox4.ShadowDecoration.Parent = this.guna2ComboBox4;
            this.guna2ComboBox4.Size = new System.Drawing.Size(261, 36);
            this.guna2ComboBox4.TabIndex = 25;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(657, 323);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(261, 20);
            this.textBox1.TabIndex = 26;
            // 
            // guna2GradientCircleButton1
            // 
            this.guna2GradientCircleButton1.CheckedState.Parent = this.guna2GradientCircleButton1;
            this.guna2GradientCircleButton1.CustomImages.Parent = this.guna2GradientCircleButton1;
            this.guna2GradientCircleButton1.FillColor2 = System.Drawing.SystemColors.HotTrack;
            this.guna2GradientCircleButton1.Font = new System.Drawing.Font("Segoe Script", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.guna2GradientCircleButton1.ForeColor = System.Drawing.Color.White;
            this.guna2GradientCircleButton1.HoverState.Parent = this.guna2GradientCircleButton1;
            this.guna2GradientCircleButton1.Location = new System.Drawing.Point(826, 540);
            this.guna2GradientCircleButton1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2GradientCircleButton1.Name = "guna2GradientCircleButton1";
            this.guna2GradientCircleButton1.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.guna2GradientCircleButton1.ShadowDecoration.Parent = this.guna2GradientCircleButton1;
            this.guna2GradientCircleButton1.Size = new System.Drawing.Size(92, 44);
            this.guna2GradientCircleButton1.TabIndex = 27;
            this.guna2GradientCircleButton1.Text = "Valider";
            // 
            // saisirDeclaration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(1122, 655);
            this.Controls.Add(this.guna2GradientCircleButton1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.guna2ComboBox4);
            this.Controls.Add(this.guna2ComboBox3);
            this.Controls.Add(this.guna2ComboBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.guna2Panel2);
            this.Controls.Add(this.guna2Panel1);
            this.Name = "saisirDeclaration";
            this.Text = "SaisirDeclaration";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormSaisir_FormClosing);
            this.guna2Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).EndInit();
            this.guna2Panel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2Button modifierDeclarationButton7;
        private Guna.UI2.WinForms.Guna2Button supprimerDeclarationButton5;
        private Guna.UI2.WinForms.Guna2Button accueilButton6;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox2;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Button consultationDeclarationButton2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox1;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox3;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox4;
        private System.Windows.Forms.TextBox textBox1;
        private Guna.UI2.WinForms.Guna2GradientCircleButton guna2GradientCircleButton1;
    }
}