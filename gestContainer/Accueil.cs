﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace gestContainer
{
    public partial class accueil : Form
    {
        public accueil()
        {
            InitializeComponent();
        }
        

        private void consulterDeclarationButton1_Click(object sender, EventArgs e)
        {
            this.Hide();
            new consultationDeclaration().ShowDialog();
            this.Close();
        }

        private void saisirDeclarationButton2_Click(object sender, EventArgs e)
        {
            this.Hide();
            new saisirDeclaration().ShowDialog();
            this.Close();

        }

        private void modifierDeclarationButton3_Click(object sender, EventArgs e)
        {

            this.Hide();
            new modifierDeclaration().ShowDialog();
            this.Close();
        }

        private void supprimerDeclarationButton4_Click(object sender, EventArgs e)
        {
            this.Hide();
            new supprimerDeclaration().ShowDialog();
            this.Close();
        }

        private void FormAccueil_FormClosing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }
    }
}
