﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace gestContainer
{
    class Conteneur
    {
        private string _numConteneur;
        private DateTime _dateAchat;
        private string _typeContainer;
        private DateTime _dateDerniereInsp;

        private bool _isNew;
        private static string _select = "select * from container";
        public Conteneur()
        {
        }

        public string NumConteneur { get => _numConteneur; }
        public DateTime DateAchat { get => _dateAchat; set => _dateAchat = value; }
        public string TypeContainer { get => _typeContainer; set => _typeContainer = value; }
        public DateTime DateDerniereInsp { get => _dateDerniereInsp; set => _dateDerniereInsp = value; }

        
        public static List<Conteneur> FetchAll()
        {

            List<Conteneur> collectionDeConteneur = new List<Conteneur>();

            MySqlConnection msc = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = msc.CreateCommand();
            commandSql.CommandText = _select;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();

            while (jeuEnregistrements.Read())
            {
                Conteneur unConteneur = new Conteneur();

                string numConteneur = jeuEnregistrements["numConteneur"].ToString();
                unConteneur._numConteneur = numConteneur;

                DateTime dateAchat = Convert.ToDateTime(jeuEnregistrements["dateAchat"].ToString());
                unConteneur._dateAchat = dateAchat;

                string typeContainer = jeuEnregistrements["typeContainer"].ToString();
                unConteneur._typeContainer = typeContainer;

                DateTime _dateDerniereInsp = Convert.ToDateTime(jeuEnregistrements["_dateDerniereInsp"].ToString());
                unConteneur._dateDerniereInsp = _dateDerniereInsp;

                unConteneur._isNew = false;
                collectionDeConteneur.Add(unConteneur);
            }
            msc.Close();//La connexion est fermée

            return collectionDeConteneur;
        }

        public static Conteneur Fetch(string numConteneur)
        {
            Conteneur unConteneur = null;

            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _select;//Définit la requete à utiliser
            commandSql.Parameters.Add(new MySqlParameter("?code", unConteneur));//Transmet un paramètre à utiliser lors de l'envoie de la requête. Il s’agit ici de l’identifiant transmis en paramètre.
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                unConteneur = new Conteneur();//Initialisation de la variable Contact
                unConteneur._numConteneur = jeuEnregistrements["Code"].ToString();//Lecture d'un champ de l'enregistrement
                unConteneur._dateAchat = Convert.ToDateTime(jeuEnregistrements["DateAchat"].ToString());
                unConteneur._typeContainer = jeuEnregistrements["typeContainer"].ToString();
                unConteneur._dateDerniereInsp = Convert.ToDateTime(jeuEnregistrements["dateDerniereInsp"].ToString());
                unConteneur._isNew = false; 
            }
            openConnection.Close();//fermeture de la connexion


            return unConteneur;
        }
    }
}
