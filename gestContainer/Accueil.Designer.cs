﻿
namespace gestContainer
{
    partial class accueil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(accueil));
            this.guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.consulterDeclarationButton1 = new Guna.UI2.WinForms.Guna2GradientCircleButton();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.saisirDeclarationButton2 = new Guna.UI2.WinForms.Guna2GradientCircleButton();
            this.guna2PictureBox2 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.modifierDeclarationButton3 = new Guna.UI2.WinForms.Guna2GradientCircleButton();
            this.guna2PictureBox4 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.supprimerDeclarationButton4 = new Guna.UI2.WinForms.Guna2GradientCircleButton();
            this.guna2PictureBox3 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2Panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox4)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Panel2
            // 
            this.guna2Panel2.BackColor = System.Drawing.SystemColors.HotTrack;
            this.guna2Panel2.Controls.Add(this.label1);
            this.guna2Panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel2.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.guna2Panel2.Name = "guna2Panel2";
            this.guna2Panel2.ShadowDecoration.Parent = this.guna2Panel2;
            this.guna2Panel2.Size = new System.Drawing.Size(1137, 170);
            this.guna2Panel2.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Segoe Script", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(472, 49);
            this.label1.MinimumSize = new System.Drawing.Size(100, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 52);
            this.label1.TabIndex = 14;
            this.label1.Text = "ACCUEIL";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.consulterDeclarationButton1);
            this.groupBox1.Controls.Add(this.guna2PictureBox1);
            this.groupBox1.Location = new System.Drawing.Point(12, 175);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(265, 386);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // consulterDeclarationButton1
            // 
            this.consulterDeclarationButton1.CheckedState.Parent = this.consulterDeclarationButton1;
            this.consulterDeclarationButton1.CustomImages.Parent = this.consulterDeclarationButton1;
            this.consulterDeclarationButton1.FillColor2 = System.Drawing.SystemColors.HotTrack;
            this.consulterDeclarationButton1.Font = new System.Drawing.Font("Segoe Script", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.consulterDeclarationButton1.ForeColor = System.Drawing.Color.White;
            this.consulterDeclarationButton1.HoverState.Parent = this.consulterDeclarationButton1;
            this.consulterDeclarationButton1.Location = new System.Drawing.Point(28, 280);
            this.consulterDeclarationButton1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.consulterDeclarationButton1.Name = "consulterDeclarationButton1";
            this.consulterDeclarationButton1.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.consulterDeclarationButton1.ShadowDecoration.Parent = this.consulterDeclarationButton1;
            this.consulterDeclarationButton1.Size = new System.Drawing.Size(210, 72);
            this.consulterDeclarationButton1.TabIndex = 8;
            this.consulterDeclarationButton1.Text = "Consulter les déclarations";
            this.consulterDeclarationButton1.Click += new System.EventHandler(this.consulterDeclarationButton1_Click);
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox1.Image")));
            this.guna2PictureBox1.Location = new System.Drawing.Point(61, 29);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.ShadowDecoration.Parent = this.guna2PictureBox1;
            this.guna2PictureBox1.Size = new System.Drawing.Size(151, 167);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox1.TabIndex = 0;
            this.guna2PictureBox1.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.saisirDeclarationButton2);
            this.groupBox2.Controls.Add(this.guna2PictureBox2);
            this.groupBox2.Location = new System.Drawing.Point(301, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(263, 386);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // saisirDeclarationButton2
            // 
            this.saisirDeclarationButton2.CheckedState.Parent = this.saisirDeclarationButton2;
            this.saisirDeclarationButton2.CustomImages.Parent = this.saisirDeclarationButton2;
            this.saisirDeclarationButton2.FillColor2 = System.Drawing.SystemColors.HotTrack;
            this.saisirDeclarationButton2.Font = new System.Drawing.Font("Segoe Script", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saisirDeclarationButton2.ForeColor = System.Drawing.Color.White;
            this.saisirDeclarationButton2.HoverState.Parent = this.saisirDeclarationButton2;
            this.saisirDeclarationButton2.Location = new System.Drawing.Point(26, 280);
            this.saisirDeclarationButton2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.saisirDeclarationButton2.Name = "saisirDeclarationButton2";
            this.saisirDeclarationButton2.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.saisirDeclarationButton2.ShadowDecoration.Parent = this.saisirDeclarationButton2;
            this.saisirDeclarationButton2.Size = new System.Drawing.Size(210, 72);
            this.saisirDeclarationButton2.TabIndex = 9;
            this.saisirDeclarationButton2.Text = "Saisir une déclaration";
            this.saisirDeclarationButton2.Click += new System.EventHandler(this.saisirDeclarationButton2_Click);
            // 
            // guna2PictureBox2
            // 
            this.guna2PictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox2.Image")));
            this.guna2PictureBox2.Location = new System.Drawing.Point(42, 41);
            this.guna2PictureBox2.Name = "guna2PictureBox2";
            this.guna2PictureBox2.ShadowDecoration.Parent = this.guna2PictureBox2;
            this.guna2PictureBox2.Size = new System.Drawing.Size(176, 155);
            this.guna2PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox2.TabIndex = 1;
            this.guna2PictureBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.modifierDeclarationButton3);
            this.groupBox3.Controls.Add(this.guna2PictureBox4);
            this.groupBox3.Location = new System.Drawing.Point(590, 175);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(255, 386);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            // 
            // modifierDeclarationButton3
            // 
            this.modifierDeclarationButton3.CheckedState.Parent = this.modifierDeclarationButton3;
            this.modifierDeclarationButton3.CustomImages.Parent = this.modifierDeclarationButton3;
            this.modifierDeclarationButton3.FillColor2 = System.Drawing.SystemColors.HotTrack;
            this.modifierDeclarationButton3.Font = new System.Drawing.Font("Segoe Script", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.modifierDeclarationButton3.ForeColor = System.Drawing.Color.White;
            this.modifierDeclarationButton3.HoverState.Parent = this.modifierDeclarationButton3;
            this.modifierDeclarationButton3.Location = new System.Drawing.Point(27, 280);
            this.modifierDeclarationButton3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.modifierDeclarationButton3.Name = "modifierDeclarationButton3";
            this.modifierDeclarationButton3.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.modifierDeclarationButton3.ShadowDecoration.Parent = this.modifierDeclarationButton3;
            this.modifierDeclarationButton3.Size = new System.Drawing.Size(210, 72);
            this.modifierDeclarationButton3.TabIndex = 10;
            this.modifierDeclarationButton3.Text = "Modifier une déclaration";
            this.modifierDeclarationButton3.Click += new System.EventHandler(this.modifierDeclarationButton3_Click);
            // 
            // guna2PictureBox4
            // 
            this.guna2PictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox4.Image")));
            this.guna2PictureBox4.Location = new System.Drawing.Point(47, 53);
            this.guna2PictureBox4.Name = "guna2PictureBox4";
            this.guna2PictureBox4.ShadowDecoration.Parent = this.guna2PictureBox4;
            this.guna2PictureBox4.Size = new System.Drawing.Size(176, 155);
            this.guna2PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox4.TabIndex = 2;
            this.guna2PictureBox4.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.supprimerDeclarationButton4);
            this.groupBox4.Controls.Add(this.guna2PictureBox3);
            this.groupBox4.Location = new System.Drawing.Point(871, 175);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(253, 386);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            // 
            // supprimerDeclarationButton4
            // 
            this.supprimerDeclarationButton4.CheckedState.Parent = this.supprimerDeclarationButton4;
            this.supprimerDeclarationButton4.CustomImages.Parent = this.supprimerDeclarationButton4;
            this.supprimerDeclarationButton4.FillColor2 = System.Drawing.SystemColors.HotTrack;
            this.supprimerDeclarationButton4.Font = new System.Drawing.Font("Segoe Script", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.supprimerDeclarationButton4.ForeColor = System.Drawing.Color.White;
            this.supprimerDeclarationButton4.HoverState.Parent = this.supprimerDeclarationButton4;
            this.supprimerDeclarationButton4.Location = new System.Drawing.Point(23, 280);
            this.supprimerDeclarationButton4.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.supprimerDeclarationButton4.Name = "supprimerDeclarationButton4";
            this.supprimerDeclarationButton4.ShadowDecoration.Mode = Guna.UI2.WinForms.Enums.ShadowMode.Circle;
            this.supprimerDeclarationButton4.ShadowDecoration.Parent = this.supprimerDeclarationButton4;
            this.supprimerDeclarationButton4.Size = new System.Drawing.Size(210, 72);
            this.supprimerDeclarationButton4.TabIndex = 11;
            this.supprimerDeclarationButton4.Text = "Supprimer une déclaration";
            this.supprimerDeclarationButton4.Click += new System.EventHandler(this.supprimerDeclarationButton4_Click);
            // 
            // guna2PictureBox3
            // 
            this.guna2PictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("guna2PictureBox3.Image")));
            this.guna2PictureBox3.Location = new System.Drawing.Point(57, 53);
            this.guna2PictureBox3.Name = "guna2PictureBox3";
            this.guna2PictureBox3.ShadowDecoration.Parent = this.guna2PictureBox3;
            this.guna2PictureBox3.Size = new System.Drawing.Size(143, 134);
            this.guna2PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.guna2PictureBox3.TabIndex = 2;
            this.guna2PictureBox3.TabStop = false;
            // 
            // accueil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.ClientSize = new System.Drawing.Size(1137, 576);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.guna2Panel2);
            this.Name = "accueil";
            this.Text = "Accueil";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormAccueil_FormClosing);
            this.guna2Panel2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox4)).EndInit();
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox2;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox3;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox4;
        private Guna.UI2.WinForms.Guna2GradientCircleButton consulterDeclarationButton1;
        private Guna.UI2.WinForms.Guna2GradientCircleButton saisirDeclarationButton2;
        private Guna.UI2.WinForms.Guna2GradientCircleButton modifierDeclarationButton3;
        private Guna.UI2.WinForms.Guna2GradientCircleButton supprimerDeclarationButton4;
    }
}